import React from 'react';
import UseState from './tutorial/1-useState/setup/2-useState-basics';
import UseStateArray from './tutorial/1-useState/setup/3-useState-array';
import UseStateObject from './tutorial/1-useState/setup/4-useState-object';
import UseStateCounter from './tutorial/1-useState/setup/5-useState-counter';
import UseEffect from './tutorial/2-useEffect/setup/1-useEffect-basics';
import UseEffectCleanup from './tutorial/2-useEffect/setup/2-useEffect-cleanup';
import UseEffectFetch from './tutorial/2-useEffect/setup/3-useEffect-fetch-data';
import ConditionalRendering from './tutorial/3-conditional-rendering/setup/1-multiple-returns';
import ConditionalRenderingShortCircuit from './tutorial/3-conditional-rendering/setup/2-short-circuit';
import ConditionalRenderingShowHide from './tutorial/3-conditional-rendering/setup/3-show-hide';
import ControlledInputs from './tutorial/4-forms/setup/1-controlled-inputs';
import MultipleInputs from './tutorial/4-forms/setup/2-multiple-inputs';
import UseRef from './tutorial/5-useRef/setup/1-useRef-basics';
import UseReduce from './tutorial/6-useReducer/setup';
import PropDrilling from './tutorial/7-prop-drilling/setup/1-prop-drilling';
import ContextApi from './tutorial/8-useContext/setup/1-context-api';
import CustomHook from './tutorial/9-custom-hooks/setup/1-fetch-example';
import PropType from './tutorial/10-prop-types/setup/index';
import NavbarG from './NavbarG';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
function App() {
  return (
    <div className='container row'>
      <Router>
        <NavbarG></NavbarG>
        {/* switch allows us to display just the first path witch matches */}
        <Switch>
          {/* exact allows us to restrict the path */}
          <Route exact path='/'>
            <UseState> </UseState>
          </Route>
          <Route path='/usestatearray'>
            <UseStateArray> </UseStateArray>
          </Route>

          <Route path='/usestateobject'>
            <UseStateObject> </UseStateObject>
          </Route>

          <Route path='/usestatecounter'>
            <UseStateCounter> </UseStateCounter>
          </Route>

          <Route path='/useeffect'>
            <UseEffect></UseEffect>
          </Route>

          <Route path='/usestatecleanup'>
            <UseEffectCleanup></UseEffectCleanup>
          </Route>

          <Route path='/useeffectfetch'>
            <UseEffectFetch></UseEffectFetch>
          </Route>

          <Route path='/conditionalrendereing'>
            <ConditionalRendering></ConditionalRendering>
          </Route>

          <Route path='/shortcircuit'>
            <ConditionalRenderingShortCircuit></ConditionalRenderingShortCircuit>
          </Route>

          <Route path='/showhide'>
            <ConditionalRenderingShowHide></ConditionalRenderingShowHide>
          </Route>

          <Route path='/controlledform'>
            <ControlledInputs></ControlledInputs>
          </Route>

          <Route path='/multipleform'>
            <MultipleInputs></MultipleInputs>
          </Route>

          <Route path='/useref'>
            <UseRef></UseRef>
          </Route>

          <Route path='/usereduce'>
            <UseReduce></UseReduce>
          </Route>

          <Route path='/propdrilling'>
            <PropDrilling></PropDrilling>
          </Route>

          <Route path='/contextapi'>
            <ContextApi></ContextApi>
          </Route>

          <Route path='/customhook'>
            <CustomHook></CustomHook>
          </Route>

          <Route path='/route'>
            <PropType></PropType>
          </Route>
        </Switch>
      </Router>
      <footer>
        <div>
          <p>
            This project is a collection of tutorials witch I used for learning
            ReactJs basics you can visit every tutorial in this page and check
            the source code bellow
          </p>
          <a href='https://bitbucket.org/MahdiLAKHAL/reactjs-advanced/src/master/'>
            <h4> // Check the source Code </h4>
          </a>
        </div>
      </footer>
    </div>
  );
}

export default App;
