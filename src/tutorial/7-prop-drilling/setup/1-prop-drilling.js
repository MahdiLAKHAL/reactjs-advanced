import React, { useState } from 'react';
import { data } from '../../../data';
import { Link } from 'react-router-dom';

// more components
// fix - context api, redux (for more complex cases)

const PropDrilling = () => {
  const [people, setPeople] = useState(data);
  const removePerson = (id) => {
    setPeople((people) => {
      return people.filter((person) => person.id !== id);
    });
  };
  return (
    <section>
      <h2>prop drilling</h2>;
      <List people={people} removePerson={removePerson}></List>
    </section>
  );
};

function List({ people, removePerson }) {
  return (
    <>
      {people.map((person) => {
        return (
          <SinglePerson
            key={person.id}
            removePerson={removePerson}
            {...person}
          ></SinglePerson>
        );
      })}
    </>
  );
}

const SinglePerson = ({ id, name, removePerson }) => {
  return (
    <div className='item'>
      <h4>{name}</h4>
      <button onClick={() => removePerson(id)}> Remove</button>
    </div>
  );
};
export default PropDrilling;
