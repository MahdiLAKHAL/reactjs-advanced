import React, { useState } from 'react';
import { Link } from 'react-router-dom';

// Use state - function
// Component name must be UpperCase
// the hook must be in the function/component body
//Cannot call conditionally
const UseStateBasics = () => {
  // console.log(useState('hello world'));

  // const value = useState(1)[0];
  // const handler = useState(1)[1];
  // console.log(value, handler);
  const [text, setText] = useState('Random Title');
  // This function allows changing title using useState
  function handlerClick(params) {
    if (text === 'Random Title') {
      setText('New Title');
    } else {
      setText('Random Title');
    }
  }
  return (
    <React.Fragment>
      <h2> {text} </h2>
      <button type='button' className='btn' onClick={handlerClick}>
        Change Title
      </button>
    </React.Fragment>
  );
};

export default UseStateBasics;
