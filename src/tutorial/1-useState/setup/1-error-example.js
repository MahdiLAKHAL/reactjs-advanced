import React from 'react';
import { Link } from 'react-router-dom';
const ErrorExample = () => {
  const title = 'Random Title';
  function handleClick() {
    title = 'hello people';
  }
  return (
    <React.Fragment>
      <h2> {title} </h2>
      <button type='button' className='btn' onClick={handleClick}>
        Change title
      </button>
    </React.Fragment>
  );
};

export default ErrorExample;
