import React from 'react';
import { data } from '../../../data';

const UseStateArray = () => {
  // we can use React.useState instead of importing the hook
  const [people, setPeople] = React.useState(data);
  function removeItem(id) {
    let newPeople = people.filter((person) => person.id !== id);
    setPeople(newPeople);
  }
  return (
    <>
      <h2> useState Array</h2>
      {people.map((person) => {
        const { id, name } = person;
        return (
          <div key={id} className='item'>
            <h4>{person.name}</h4>
            <button onClick={() => removeItem(id)}>remove</button>
          </div>
        );
      })}
      <button className='btn' onClick={() => setPeople([])}>
        Clear items
      </button>
    </>
  );
};

export default UseStateArray;
