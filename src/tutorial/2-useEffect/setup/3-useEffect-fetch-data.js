import React, { useState, useEffect } from 'react';

const url = 'https://api.github.com/users';

//second argument

const UseEffectFetchData = () => {
  const [users, setUsers] = useState([]);

  const getUsers = async () => {
    const response = await fetch(url);
    const users = await response.json();
    setUsers(users);
    console.log(users);
  };

  useEffect(() => {
    getUsers();

    // we're using the empty array to not go into an infinit loop
    // the useState trigger re-render every time we call setUsers and then it update users after getting them
  }, []);
  return (
    <>
      <h1> Github users</h1>
      <ul className='users'>
        {users.map((user) => {
          const { id, login, avatar_url, html_url } = user;
          return (
            <li key={id}>
              <img src={avatar_url} alt={login} />
              <div>
                <h4> {login} </h4>
                <a href={html_url}> Visit </a>
              </div>
            </li>
          );
        })}
      </ul>
    </>
  );
};

export default UseEffectFetchData;
