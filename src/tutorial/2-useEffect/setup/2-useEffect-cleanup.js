import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

// cleanup function
// second argument

const UseEffectCleanup = () => {
  const [size, setSize] = useState(window.innerWidth);
  console.log(size);

  function checkSize() {
    setSize(window.innerWidth);
  }

  useEffect(() => {
    //This function allows checking the size in real time
    window.addEventListener('resize', checkSize);

    // this function allows us to clean up the event listener every time re-rendered
    return () => {
      console.log('cleanup');
      window.removeEventListener('resize', checkSize);
    };
  });

  return (
    <>
      <h1>Window</h1>
      <h2> {size} PX </h2>
    </>
  );
};

export default UseEffectCleanup;
