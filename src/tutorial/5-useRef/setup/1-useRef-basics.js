import React, { useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';

// preserves value
// DOES NOT trigger re-render
// target DOM nodes/elements

const UseRefBasics = () => {
  // UseRef allows to access any Dom element having the ref
  const refContainer = useRef(null);
  function handleSubmit(e) {
    e.preventDefault();
    console.log(refContainer.current.value);
  }
  console.log(refContainer);

  useEffect(() => {
    refContainer.current.focus();
  });
  return (
    <>
      <h2>UseRef Basics</h2>
      <form className='form' onSubmit={handleSubmit}>
        <div>
          <input type='text' ref={refContainer} />
          <button type='submit'>Submit</button>
        </div>
      </form>
    </>
  );
};

export default UseRefBasics;
