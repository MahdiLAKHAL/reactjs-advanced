import React from 'react';
import { Link } from 'react-router-dom';
const NavbarG = () => {
  return (
    <>
      <nav className='nav'>
        <div className='nav-header'>
          <ul className='nav-links nav-center'>
            <li>
              <Link to='/'> useState tutorial</Link>
            </li>
            <li>
              <Link to='/usestatearray'>useState Array Tutorial </Link>
            </li>
            <li>
              <Link to='/usestateobject'>useState Object Tutorial</Link>
            </li>
            <li>
              <Link to='/usestatecounter'>useState Counter Tutorial</Link>
            </li>
            <li>
              <Link to='/useeffect'>useEffect Tutorial</Link>
            </li>
            <li>
              <Link to='/usestatecleanup'>useState Cleanup Tutorial</Link>
            </li>
            <li>
              <Link to='/useeffectfetch'>useEffect Fetch Tutorial</Link>
            </li>
            <li>
              <Link to='/conditionalrendereing'>
                conditional Rendereing Tutorial
              </Link>
            </li>
            <li>
              <Link to='/shortcircuit'>
                conditional Rendereing 'shortCircuit' Tutorial
              </Link>
            </li>
            <li>
              <Link to='/showhide'>
                conditional Rendereing 'show and hide' Tutorial
              </Link>
            </li>
            <li>
              <Link to='/controlledform'>controlled Inputs Tutorial</Link>
            </li>
            <li>
              <Link to='/multipleform'>multiple Inputs Tutorial</Link>
            </li>
            <li>
              <Link to='/useref'>useRef Tutorial</Link>
            </li>
            <li>
              <Link to='/usereduce'>useReduce Tutorial</Link>
            </li>
            <li>
              <Link to='/propdrilling'>prop Drilling Tutorial</Link>
            </li>
            <li>
              <Link to='/contextapi'>context Api Tutorial</Link>
            </li>
            <li>
              <Link to='/customhook'>custom Hook Tutorial</Link>
            </li>
            <li>
              <Link to='/route'>React Router Tutorial</Link>
            </li>
            <li>
              <Link to='/route'>React Router Tutorial</Link>
            </li>
          </ul>
        </div>
      </nav>
    </>
  );
};

export default NavbarG;
